# UHCFR
This website references a few information for the UHCFR team.
See on Twitter: [@UHCFR](https://twitter.com/UHCFr)
## Website
If you need more information about keep maintaining this website, please contact Shams Golap <shams79000@gmail.com>.
# Licence
This website is under the terms of the WTFPL. 
Check out the [LICENCE.md](LICENCE.md) file
